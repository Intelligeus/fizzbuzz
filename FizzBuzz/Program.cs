﻿using System;

namespace FizzBuzz
{
    class Program
    {
        // The first part of the test is to write a simple console
        // app for the FizzBuzz challenge so we do this as simply
        // as possible without using too many clauses for readability
        static void Main(string[] args) 
        {
            for (int number = 1; number < 101; number++)
            {
                bool isFizz = IsMultiple(number, 3);
                bool isBuzz = IsMultiple(number, 5);

                Console.WriteLine(isFizz ? isBuzz ? "FizzBuzz" : "Fizz" : isBuzz ? "Buzz" : number.ToString());
            }
        }


        public static bool IsMultiple(int number, int divisor)
        {
            return number % divisor == 0;
        }
    }
}
