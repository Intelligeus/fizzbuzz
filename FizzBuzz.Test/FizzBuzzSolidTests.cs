﻿using FizzBuzz.Lib;
using NUnit.Framework;

namespace FizzBuzz.Test
{
    [TestFixture]
    public class FizzBuzzSolidTests
    {
        // For the solid aproach we follow a similar structure to TDD
        // get the simplest possible testcase working and progress. We just have a 
        // different structure in the implementation
        [Test]
        public void ShouldGetNumberForNumber()
        {
            // the easiest test we can run is to get back a hardcoded value
            IFizzBuzz fizzBuzz = new FizzBuzzSolid();
            var result = fizzBuzz.CheckNumber(1);
            Assert.AreEqual("1", result);
        }

        [Test]
        public void ShouldGetNumberForNumbers([Values(2, 4, 16, 32, 41, 88)] int number)
        {
            // Now we need to test with more numbers to get their actual value
            IFizzBuzz fizzBuzz = new FizzBuzzSolid();
            var result = fizzBuzz.CheckNumber(number);
            Assert.AreEqual(number.ToString(), result);
        }

        [Test]
        public void ShouldGetFizzForMultipleOfThree()
        {
            // Move on to a more complete SOLID aproach for the rest of the functionality
            // We can just inject straight in without using a DI container here
            INumberAnalyser analyzer = new NumberAnalyser(new FizzBuzzSolid());
            var result = analyzer.AnalyseNumber(3);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void ShouldGetFizzForMultiplesOfThree([Values(9, 21, 33, 48, 96)] int number)
        {
            // Now test some additional cases
            INumberAnalyser analyzer = new NumberAnalyser(new FizzBuzzSolid());
            var result = analyzer.AnalyseNumber(number);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void ShouldGetBuzzForMultipleOfFive()
        {
            // Now repeat the process for Buzz Numbers
            INumberAnalyser analyzer = new NumberAnalyser(new FizzBuzzSolid());
            var result = analyzer.AnalyseNumber(5);
            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void ShouldGetBuzzForMultiplesOfFive([Values(10, 25, 50, 70, 80)] int number)
        {
            // And again test some additional cases to verify correctness
            INumberAnalyser analyzer = new NumberAnalyser(new FizzBuzzSolid());
            var result = analyzer.AnalyseNumber(number);
            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void ShouldGetFizzBuzzForMultipleOfThreeAndFive()
        {
            // And lastly we check the FizzBuzz case
            INumberAnalyser analyzer = new NumberAnalyser(new FizzBuzzSolid());
            var result = analyzer.AnalyseNumber(15);
            Assert.AreEqual("FizzBuzz", result);
        }

        [Test]
        public void ShouldGetFizzBuzzForMultiplesOfThreeAndFive([Values(30, 45, 75, 90)] int number)
        {
            // And verify with some additional cases
            INumberAnalyser analyzer = new NumberAnalyser(new FizzBuzzSolid());
            var result = analyzer.AnalyseNumber(number);
            Assert.AreEqual("FizzBuzz", result);
        }
    }
}
