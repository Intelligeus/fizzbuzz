﻿using System;
using FizzBuzz.Lib;
using NUnit.Framework;

namespace FizzBuzz.Test
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [Test]
        public void ShouldGetValidResponseForNumber()
        {
            // OK the simplest test we can start with is to just return a string
            // for a number so lets try that

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(1);
            Assert.AreEqual("1", result);
        }


        [Test]
        public void ShouldGetFizzForMultipleOfThree()
        {
           // Now try and get the simplest case for Fizz

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(3);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void ShouldGetFizzForMultiplesOfThree([Values(6, 18, 27, 66, 93)] int number)
        {
            // OK that worked so test for some more values

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(number);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void ShouldGetBuzzForMultipleOfFive()
        {
            // Now test for the simplest Buzz case

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(5);
            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void ShouldGetBuzzForMultiplesOfFive([Values(10, 20, 35, 70, 95)] int number)
        {
            // OK that worked so again test for some more values

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(number);
            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void ShouldGetFizzBuzzForMultipleOfThreeAndFive()
        {
            // Now we need to handle multiples of Three and Five
    
            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(15);
            Assert.AreEqual("FizzBuzz", result);
        }


        [Test]
        public void ShouldGetFizzBuzzForMultiplesOfThreeAndFive([Values(15, 30, 60, 90)] int number)
        {
            // And again verify for some more values

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(number);
            Assert.AreEqual("FizzBuzz", result);
        }

        [Test]
        public void ShouldGetNumberForNumbers([Values(4, 17, 26, 71, 88)] int number)
        {
            // Finally sanity check some regular numbers again

            FizzBuzzTDD fizzBuzz = new FizzBuzzTDD();
            string result = fizzBuzz.CheckNumber(number);
            Assert.AreEqual(number.ToString(), result);
        }


    }
}
