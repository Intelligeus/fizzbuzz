﻿using System;
using System.Collections.Generic;
using System.Web.Http.Results;
using FizzBuzz.Lib;
using FizzBuzz.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace FizzBuzz.Test
{
    [TestFixture]
    public class FizzBuzzAPITests
    {
        // Start off by getting the controller to return a hard coded string
        [Test]
        public void FizzBuzzDefaultActionShouldReturnSomething()
        {
            // Mock this out before we actually set up the DI container
            // We add an extra interface just for cleaner seperation of concerns
            // although it's not strictly necessary
            var analyser = new Mock<INumberAnalyser>();
            var data = new Mock<IFizzBuzzData>();
            data.Setup(x => x.GetFizzBuzzData()).Returns(new List<string>());
            var controller = new FizzBuzzController(data.Object, analyser.Object);
            var result = controller.Default();

            Assert.IsNotNull(result);
        }

        [Test]
        public void FizzBuzzDefaultActionShouldReturnProperData()
        {
            // Try it again with the correct functionality injected directly
            var analyser = new Mock<INumberAnalyser>();
            var controller = new FizzBuzzController(new FizzBuzzData(), analyser.Object);
            var result = controller.Default();

            var contentResult = result as OkNegotiatedContentResult<IList<string>>;
            var data = contentResult.Content;
            Assert.IsNotNull(result);
            Assert.AreEqual(100, data.Count);
            Assert.AreEqual("1", data[0]);
            Assert.AreEqual("Fizz", data[2]);
            Assert.AreEqual("Buzz", data[4]);
            Assert.AreEqual("FizzBuzz", data[14]);

        }

        [Test]
        public void FizzBuzzDefaultActionShouldReturnNumberForNumber()
        {
            // The default action works so now move on and pass in a value
            // Again just mocked till the DI is setup
            var data = new Mock<IFizzBuzzData>();
            data.Setup(x => x.GetFizzBuzzData()).Returns(new List<string>());
            var analyser = new Mock<INumberAnalyser>();
            analyser.Setup(x => x.AnalyseNumber(It.IsAny<int>())).Returns("1");

            var controller = new FizzBuzzController(data.Object, analyser.Object);
            var result = controller.Default(1);
            var contentResult = result as OkNegotiatedContentResult<string>;

            Assert.AreEqual("1", contentResult.Content);
        }

        [Test]
        public void FizzBuzzDefaultActionShouldReturnNumberForNumbers([Values(1, 4, 8, 23, 37)] int number)
        {
            // We can try this again now and inject the number analyser directly
            var data = new Mock<IFizzBuzzData>();
            data.Setup(x => x.GetFizzBuzzData()).Returns(new List<string>());

            var controller = new FizzBuzzController(data.Object, new NumberAnalyser(new FizzBuzzSolid()));
            var result = controller.Default(number);
            var contentResult = result as OkNegotiatedContentResult<string>;

            Assert.AreEqual(number.ToString(), contentResult.Content);
        }

        [Test]
        public void FizzBuzzDefaultActionShouldReturnFizzForMultiplesOfThree([Values(6, 12, 21, 63, 96)] int number)
        {
            // Test again for multiples of three
            var data = new Mock<IFizzBuzzData>();
            data.Setup(x => x.GetFizzBuzzData()).Returns(new List<string>());

            var controller = new FizzBuzzController(data.Object, new NumberAnalyser(new FizzBuzzSolid()));
            var result = controller.Default(number);
            var contentResult = result as OkNegotiatedContentResult<string>;
            Assert.AreEqual("Fizz", contentResult.Content);
        }

        [Test]
        public void FizzBuzzDefaultActionShouldReturnBuzzForMultiplesOfFive([Values(10, 25, 40, 70, 85)] int number)
        {
            // Test multiples of five
            var data = new Mock<IFizzBuzzData>();
            data.Setup(x => x.GetFizzBuzzData()).Returns(new List<string>());

            var controller = new FizzBuzzController(data.Object, new NumberAnalyser(new FizzBuzzSolid()));
            var result = controller.Default(number);
            var contentResult = result as OkNegotiatedContentResult<string>;
            
            Assert.AreEqual("Buzz", contentResult.Content);
        }


        [Test]
        public void FizzBuzzDefaultActionShouldReturnFizzBuzzForMultiplesOfThreeAndFive([Values(15, 30, 45, 75, 90)] int number)
        {
            // And finally multiples of three and five
            var data = new Mock<IFizzBuzzData>();
            data.Setup(x => x.GetFizzBuzzData()).Returns(new List<string>());

            var controller = new FizzBuzzController(data.Object, new NumberAnalyser(new FizzBuzzSolid()));
            var result = controller.Default(number);
            var contentResult = result as OkNegotiatedContentResult<string>;

            Assert.AreEqual("FizzBuzz", contentResult.Content);
        }

    }
}
