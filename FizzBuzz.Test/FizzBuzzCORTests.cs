﻿using System;
using FizzBuzz.Lib;
using NUnit.Framework;

namespace FizzBuzz.Test
{
    [TestFixture]
    public class FizzBuzzCORTests
    {
        [Test]
        public void ShouldGetNumberForNumber()
        {
            // Again as with the other approches we start 
            // with the simplest case we can pass.
            
            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(1);
            Assert.AreEqual("1", result);
        }

        [Test]
        public void ShouldGetNumberForNumbers([Values(4, 8, 16, 31, 56, 97)] int number)
        {
            // And extend that test for other numbers

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(number);
            Assert.AreEqual(number.ToString(), result);
        }

        [Test]
        public void ShouldGetFizzForMultipleOfThree()
        {
            // Now we need a Fizz handler for the chain

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(3);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void ShouldGetFizzForMultiplesOfThree([Values(6, 12, 21, 39, 78)] int number)
        {
            // And test with more values to confirm

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(number);
            Assert.AreEqual("Fizz", result);
        }

        [Test]
        public void ShouldGetBuzzForMultipleOfFive()
        {
            // Now we need a Buzz handler for the chain

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(5);
            Assert.AreEqual("Buzz", result);
        }


        [Test]
        public void ShouldGetBuzzForMultiplesOfFive([Values(10, 25, 50, 80, 85)] int number)
        {
            // And again confirm our results

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(5);
            Assert.AreEqual("Buzz", result);
        }

        [Test]
        public void ShouldGetFizzBuzzForMultipleOfThreeAndFive()
        {
            // Now we need a Buzz handler for the chain

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(15);
            Assert.AreEqual("FizzBuzz", result);
        }

        [Test]
        public void ShouldGetFizzBuzzForMultiplesOfThreeAndFive([Values(30, 45, 75, 90)] int number)
        {
            // And validate for some more cases so we can refactor

            FizzBuzzCOR fizzBuzz = new FizzBuzzCOR();
            var result = fizzBuzz.AnalyseNumber(number);
            Assert.AreEqual("FizzBuzz", result);
        }
    }
}
