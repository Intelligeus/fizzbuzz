﻿
using System.Collections.Generic;
using System.Web.Http;
using FizzBuzz.Lib;

namespace FizzBuzz.Web.Controllers
{
    public class FizzBuzzController : ApiController
    {
        private IFizzBuzzData data;
        private INumberAnalyser analyser;
        public FizzBuzzController(IFizzBuzzData data, INumberAnalyser analyser)
        {
            this.data = data;
            this.analyser = analyser;
        }

        [Route("api/v1/fizzbuzz")]
        [HttpGet]
        public IHttpActionResult Default()
        {
            return Ok(data.GetFizzBuzzData());
        }

        // Use routing to overload the default method
        [Route("api/v1/fizzbuzz/{number:int}")]
        [HttpGet]
        public IHttpActionResult Default(int number)
        {
            return Ok(analyser.AnalyseNumber(number));
        }
    }
}
