﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FizzBuzz.Lib;
using FizzBuzz.Web.Resolver;
using Unity;
using Unity.Lifetime;

namespace FizzBuzz.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<INumberAnalyser, NumberAnalyser>(new TransientLifetimeManager());
            container.RegisterType<IFizzBuzzData, FizzBuzzData>(new TransientLifetimeManager());
            container.RegisterType<IFizzBuzz, FizzBuzzSolid>(new TransientLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            // Make Json our default return type and set the correct header
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings
                .Add(new System.Net.Http.Formatting.RequestHeaderMapping("Accept",
                    "text/html",
                    StringComparison.InvariantCultureIgnoreCase,
                    true,
                    "application/json"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/v1/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
